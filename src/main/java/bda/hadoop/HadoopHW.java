package bda.hadoop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.io.compress.SnappyCodec;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/**
 * 
 * @author Ирина Румянцева
 *
 */

public class HadoopHW {

	/**
	 * @param args
	 *            , первый аргумент - входящий файл, второй аргумент - выходящий
	 *            файл
	 * @return код заверешения программы
	 */
	public static void main(String[] args) throws Exception {

		if (args.length != 2) {
			System.err.println("Usage. args:<in> <out>");
			System.exit(2);
		}

		Configuration conf = new Configuration();

		Job job = new Job(conf);
		job.setJarByClass(HadoopHW.class);

		job.setMapperClass(HadoopHWMapper.class);
		job.setReducerClass(HadoopHWReducer.class);

		job.setNumReduceTasks(2);

		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		
		FileInputFormat.addInputPath(job, new Path(args[0]));

		
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		FileOutputFormat.setCompressOutput(job, true);
		FileOutputFormat.setOutputCompressorClass(job, SnappyCodec.class);

		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}

}