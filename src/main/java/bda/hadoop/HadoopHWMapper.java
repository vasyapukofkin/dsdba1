package bda.hadoop;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 * 
 * @author Ирина Румянцева
 *
 */

public class HadoopHWMapper extends Mapper<Object, Text, Text, IntWritable> {

	private final static IntWritable one = new IntWritable(1);
	private Text url = new Text();

	private Pattern p = Pattern.compile("(\"[^\"]*\")$");

	/**
	 * @param Ключ
	 *            пары ключ-значение
	 * @param Значение
	 *            пары ключ-значение
	 * @param Контекст
	 *            mapper'а
	 * 
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void map(Object key, Text value, Context context)
			throws IOException, InterruptedException {
		Matcher matcher = p.matcher(value.toString());
		String browser = null;
		if (matcher.find()) {

			if (matcher.group(0).contains("Firefox")
					&& !matcher.group(0).contains("Seamonkey")) {
				browser = "Firefox";
			}
			if (matcher.group(0).contains("Seamonkey")) {
				browser = "Seamonkey";
			}
			if (matcher.group(0).contains("Chrome")
					&& !matcher.group(0).contains("Chromium")) {
				browser = "Chrome";
			}
			if (matcher.group(0).contains("Chromium")) {
				browser = "Chromium";
			}
			if (matcher.group(0).contains("Safari")
					&& !matcher.group(0).contains("Chrome")
					&& !matcher.group(0).contains("Chromium")) {
				browser = "Safari";
			}
			if (matcher.group(0).contains("OPR")
					|| matcher.group(0).contains("Opera")) {
				browser = "Opera";
			}
			if (matcher.group(0).contains("MSIE")) {
				browser = "MSIE";
			}
		}

		if (browser != null) {
			url.set(browser);
			context.write(url, one);
		}

	}
}