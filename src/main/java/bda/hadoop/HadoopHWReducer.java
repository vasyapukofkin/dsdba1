package bda.hadoop;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
 * 
 * @author Ирина Румянцева
 *
 */

public class HadoopHWReducer extends
		Reducer<Text, IntWritable, Text, IntWritable> {

	private IntWritable total = new IntWritable();

	/**
	 * @param ключ
	 * @param список
	 *            значений
	 * @param Контекст
	 *            reducer'а
	 * 
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void reduce(Text key, Iterable<IntWritable> values, Context context)
			throws IOException, InterruptedException {
		int sum = 0;
		for (IntWritable value : values) {
			sum += value.get();
		}
		total.set(sum);
		context.write(key, total);
	}
}