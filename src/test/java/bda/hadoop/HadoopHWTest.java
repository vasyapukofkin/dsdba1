package bda.hadoop;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import bda.hadoop.HadoopHWMapper;
import bda.hadoop.HadoopHWReducer;

public class HadoopHWTest {

	private MapDriver<Object, Text, Text, IntWritable> mapDriver;
	private ReduceDriver<Text, IntWritable, Text, IntWritable> reduceDriver;
	private MapReduceDriver<Object, Text, Text, IntWritable, Text, IntWritable> mapReduceDriver;
	private Properties file1;
	private Properties file2;
	
	@Before
	public void init() throws FileNotFoundException, IOException {
		HadoopHWMapper mapper = new HadoopHWMapper();
		HadoopHWReducer reducer = new HadoopHWReducer();
		mapDriver = MapDriver.newMapDriver(mapper);
		reduceDriver = ReduceDriver.newReduceDriver(reducer);
		mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
		this.file1 = new Properties();
		file1.load(new FileReader("./src/test/resources/test_file1.properties"));
		this.file2 = new Properties();
		file2.load(new FileReader("./src/test/resources/test_file2.properties"));
	}

	@Test
	public void testMapper1() {
		mapDriver.withInput(new Object(), new Text(file1.getProperty("mapper.row")));
		mapDriver.withOutput(new Text(file1.getProperty("mapper.browser")), new IntWritable(1));
		mapDriver.runTest();
	}

	@Test
	public void testReducer1() {
		List<IntWritable> values = new ArrayList<IntWritable>();
		values.add(new IntWritable(1));
		values.add(new IntWritable(1));
		reduceDriver.withInput(new Text(file1.getProperty("reducer.input")), values);
		reduceDriver.withOutput(new Text(file1.getProperty("reducer.output")), new IntWritable(2));
		reduceDriver.runTest();
	}

	@Test
	public void testMapReduce1() {
		mapReduceDriver.addInput(new Object(),new Text(file1.getProperty("program.row1")));
		mapReduceDriver.addInput(new Object(),new Text(file1.getProperty("program.row2")));
		mapReduceDriver.withOutput(new Text(file1.getProperty("program.browser")), new IntWritable(2));
		mapReduceDriver.runTest();
	}
	
	@Test
	public void testMapper2() {
		mapDriver.withInput(new Object(), new Text(file2.getProperty("mapper.row")));
		mapDriver.withOutput(new Text(file2.getProperty("mapper.browser")), new IntWritable(1));
		mapDriver.runTest();
	}

	@Test
	public void testReducer2() {
		List<IntWritable> values = new ArrayList<IntWritable>();
		values.add(new IntWritable(1));
		values.add(new IntWritable(1));
		reduceDriver.withInput(new Text(file2.getProperty("reducer.input")), values);
		reduceDriver.withOutput(new Text(file2.getProperty("reducer.output")), new IntWritable(2));
		reduceDriver.runTest();
	}

	@Test
	public void testMapReduce2() {
		mapReduceDriver.addInput(new Object(),new Text(file2.getProperty("program.row1")));
		mapReduceDriver.addInput(new Object(),new Text(file2.getProperty("program.row2")));
		mapReduceDriver.withOutput(new Text(file2.getProperty("program.browser")), new IntWritable(2));
		mapReduceDriver.runTest();
	}

}
